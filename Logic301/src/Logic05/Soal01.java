package Logic05;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input banyak bilangan yang akan ditampilkan: ");
        int bilangan = input.nextInt();

        /*String[] numberStringArray = bilangan.split(" ");
        int[] numberInt = new int[numberStringArray.length];

        //Masukkan nilai ke dalam array
        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
        }*/

        //deret ganjil
        for (int i = 1; i <= bilangan; i++) {
            if(i%2!=0){
                System.out.print(i);
            }
        }
        //deret genap
        System.out.println();
        for (int i = 1; i <= bilangan; i++) {
            if(i%2==0){
                System.out.print(i);
            }
        }
    }
}
