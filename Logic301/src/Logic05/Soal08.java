package Logic05;

import java.util.Scanner;

public class Soal08 {
    public Soal08() {
    }

    public static void Resolve() {
        //penjumlahan masing-masing array deret fibo dan deret prima
        Scanner input = new Scanner(System.in);
        System.out.print("please enter the values of n = ");
        int batasan = input.nextInt();
        
        //deklarasi untuk bilangan fibonaci
        int fibonaci = 1; //fibo mulai dari 1
        int bilanganAwal = 0;
        int bialnganAkhir = 1; //simpan nilai 1, hubungannya dengan rumus mencari rumus fibo

        //deklarasi untuk bilangan prima

        
        //inisialisasi ke array
        int[] array = new int[batasan];
        int[] arrayFibonaci = new int[batasan];
        int[] arrayPrima = new int[batasan];
        int[] arraySum = new int[batasan];

        //deret fibo
        int i;
        for(i = 0; i < array.length; ++i) {
            System.out.print(fibonaci + " ");
            arrayFibonaci[i] = fibonaci;
            //rumus fibo
            fibonaci = bilanganAwal + bialnganAkhir;
            bilanganAwal = bialnganAkhir;
            bialnganAkhir = fibonaci;
        }

        System.out.println();
         int helper=0;

        i = 1;
        //deret prima
        while(true) { //eksekusi untuk dapat nilai deret prima dan panjang array yang akan dijumlahkan
            int number = 0; //helper untuk simpan angka untuk cari bil. prima
            for(int j = 1; j <= i; j++) {
                if (i % j == 0) { //bilangan prima
                    ++number;
                }
            }

            if (number == 2) { //bil. prima itu hanya dapat habis dibagi dengan dirinya sendiri atau 1
                arrayPrima[helper] = i;
                helper++;
                System.out.print(i + " ");
                if (helper == array.length) {
                    System.out.println();
                    System.out.println();

                    //penjumlahan array deret fibo dan array deret prima
                    for(i = 0; i < array.length; ++i) {
                        arraySum[i] = arrayFibonaci[i] + arrayPrima[i];
                        System.out.print(arraySum[i] + " ");
                    }

                    break;
                }
            }
            i++;
        }
    }
}
