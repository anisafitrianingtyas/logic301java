package Logic05;

import Logic03.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while (answer.toUpperCase().equals("Y")) {
            System.out.print("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1:
                    Soal01.Resolve(); //ganjil genap
                    break;
                case 2:
                    Soal02.Resolve(); //huruf vokal n konsonan
                    break;
                case 3:
                    Soal03.Resolve(); //100 103 109
                    break;
                case 4:
                    Soal04.Resolve(); //hitung bensin
                    break;
                case 5:
                    Soal05.Resolve(); //hitung porsi
                    break;
                case 6:
                    Soal06.Resolve(); //transfer atm
                    break;
                case 7:
                    Soal07.Resolve(); //kartu atau gambaran
                    break;
                case 8:
                    Soal08.Resolve(); //penjumlahan bilangan prima dan bilangan fibonacci
                    break;
                case 9:
                    Soal09.Resolve(); //Perjalanan melewati gunung
                    break;
                case 10:
                    Soal10.Resolve(); //saldo ovo setelah beli
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }
                System.out.println(" ");
                System.out.println("Again? (Y/N)");
                input.nextLine();
                answer = input.nextLine();
            }

            System.out.println("Done");
        }
    }
