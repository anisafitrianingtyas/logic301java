package Logic05;
import java.util.Scanner;

public class Soal06 {
    public Soal06() {
    }

    public static void Resolve() {
        String pin = "123456";
        long saldo = 0L;

        char ulangTransaksi;
        do {
            Scanner input = new Scanner(System.in);
            System.out.print("Input PIN = ");
            String inputPin = input.nextLine();
            if (pin.equals(inputPin)) { //bandingkan value string pakai equals
                System.out.println("Masukkan Nominal Setor tunai");
                long inputSetor = (long)input.nextInt();
                saldo += inputSetor;
                System.out.println("Setor Tunai Berhasil");
                System.out.println("Saldo anda saat ini : " + saldo);
                System.out.println();
                System.out.println("1.Antar Rekening 2.Antar Bank");
                System.out.println("Input pilihan berupa angka: ");
                int menuTransfer = input.nextInt();
                if (menuTransfer == 1) {
                    System.out.println("Transfer Antar Rekening");
                    System.out.println("Masukan Nomor Rekening Tujuan");
                    long noRek1 = input.nextLong();
                    System.out.println("Masukan Nominal Transfer");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer >= saldo) {
                        System.out.println("Saldo Tidak Mencukupi");
                    } else if (nominalTransfer <= saldo) {
                        System.out.println("Tranfer ke Nomor Rekening : " + noRek1);
                        System.out.println("Jumlah Nominal transfer : " + nominalTransfer);
                        System.out.println("Transaksi berhasil");
                        saldo -= nominalTransfer;
                        System.out.println("Saldo Anda saat ini : " + saldo);
                    }
                } else if (menuTransfer == 2) {
                    System.out.println("Transfer Antar Bank");
                    System.out.println("Masukan kode Bank");
                    int kodeBank = input.nextInt();
                    System.out.println("Masukan Nomor Rekening Tujuan");
                    long noRek1 = input.nextLong();
                    System.out.println("Masukan Nominal Transfer");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer + 7500L >= saldo) {
                        System.out.println("Saldo Tidak Cukup");
                    } else if (nominalTransfer <= saldo + 7500L) {
                        System.out.println("Tranfer ke Nomor Rekening : " + noRek1);
                        System.out.println("Jumlah Nominal transfer : " + nominalTransfer);
                        System.out.println("Transaksi berhasil");
                        saldo -= nominalTransfer + 7500L;
                        System.out.println("Saldo Anda saat ini : " + saldo);
                    }
                }
            } else {
                System.out.println("PIN yang Anda masukan salah");
            }

            System.out.println("Ingin transaksi lagi! (y/n)");
            ulangTransaksi = input.next().charAt(0);
        } while(ulangTransaksi == 'y' && ulangTransaksi != 'n');

    }
}
