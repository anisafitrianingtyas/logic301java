package Logic05;
import java.util.Random;
import java.util.Scanner;

public class Soal07 {
    public Soal07() {
    }

    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Jumlah Kartu Atau Gambaran = ");
        int kartu = input.nextInt();

        while(kartu > 0) {
            System.out.println("Masukkan Jumlah Tawaran = ");

            int tawaran;
            for(tawaran = input.nextInt(); tawaran > kartu; tawaran = input.nextInt()) {
                System.out.println("Tidak bisa. Masukkan jumlah tawaran kurang dari jumlah kartu Anda");
                System.out.println("Masukkan Jumlah Tawaran = ");
            }

            //deklarasi variable
            int kotakA = 0;
            int kotakB = 0;
            int pilihan = 0;
            int lawan = 0;
            String pesan = " ";
            String menyerah = " ";
            System.out.println();

            //Pilih antara kotak A dan kotak B
            Random random = new Random(); //ambil nilai random
            int random1 = random.nextInt(10);
            int random2 = random.nextInt(10);
            System.out.println("Pilihan Kotak  A atau B ?");
            char kotak = input.next().charAt(0);
            if (kotak == 'B' || kotak == 'b') {
                kotakB = random2;
                pilihan = random2;
                lawan = random1;
                kotakA = random1;
            } else if (kotak == 'A' || kotak == 'a'){
                kotakA = random1;
                pilihan = random1;
                lawan = random2;
                kotakB = random2;
            } else{
                System.out.println("Input salah.");
            }

            if (pilihan < lawan) {
                kartu -= tawaran;
                pesan = "You Lose";
            } else if (pilihan > lawan) {
                kartu += tawaran;
                pesan = "You Win";
            } else {
                System.out.println("Draw");
            }

            System.out.println("nilai kotak A = " + kotakA + " Kotak B = " + kotakB + "");
            System.out.println("Kartu = " + kartu);
            System.out.println(pesan);
            if (kartu != 0 && kartu >= 0) {
                System.out.println("Kartu Anda Saat ini : " + kartu);
                System.out.println("Anda Menyerah?");
                input.nextLine();
                System.out.println("Y / N");
                menyerah = input.nextLine();
            } else {
                System.out.println("Maaf, Kartu habis atau Anda kalah");
            }

            if (menyerah.equals("y") || menyerah.equals("Y")) {
                break;
            }
        }

    }
}
