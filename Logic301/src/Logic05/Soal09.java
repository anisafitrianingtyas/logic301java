package Logic05;
import java.util.Scanner;

public class Soal09 {
    public Soal09() {
    }

    public static void Resolve() {
        System.out.println("Deskripsi simbol: ");
        System.out.println("N = naik, T = turun");
        System.out.println("\n");
        //Input pola perjalanan
        System.out.print("Input pola : ");
        Scanner input = new Scanner(System.in);
        String pola = input.nextLine();
        char[] arrayPola = pola.toCharArray();

        //deklarasi
        int mdpl = 0;
        int gunung = 0;
        int lembah = 0;

        //dihitung 1 gunung jika NT dan lembah dihitung jika ex TN
        for(int i = 0; i < arrayPola.length; i++) {
            if (arrayPola[i] == 'N') {
                mdpl++;
                if (mdpl == 0) {
                    ++lembah;
                }
            } else if (arrayPola[i] == 'T') {
                mdpl--;
                if (mdpl == 0) {
                    ++gunung;
                }
            }
        }

        for(int i = 0; i < arrayPola.length; i++) {
            System.out.print(arrayPola[i] + " ");
        }

        System.out.println("");
        System.out.print("Hattori melewati " + gunung + " gunung dan " + lembah + " lembah");
    }
}

