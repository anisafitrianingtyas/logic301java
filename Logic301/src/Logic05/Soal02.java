package Logic05;

import java.util.Arrays;
import java.util.Scanner;

public class Soal02 {
    public static void Resolve(){
        //Input words
        Scanner input = new Scanner(System.in);
        System.out.print("Input words: ");
        String words = input.nextLine();

        //count huruf kecil + spasi
        words = words.toLowerCase();
        String[] wordsArray = words.split(" ");

        //Inisialisasi untuk vokal dan konsonan
        String vokal="";
        String konsonan="";

        //cari vokal dan konsonan
        for (int i = 0; i < wordsArray.length; i++) {
            for (int j = 0; j < wordsArray[i].length(); j++) {
                //kondisi cari huruf vokal
                if (wordsArray[i].charAt(j) == 'a' || wordsArray[i].charAt(j) == 'e' ||
                        wordsArray[i].charAt(j) == 'i' || wordsArray[i].charAt(j) == 'o' ||
                        wordsArray[i].charAt(j) == 'u') {
                    vokal = vokal + wordsArray[i].charAt(j);
                } else {
                    konsonan = konsonan + wordsArray[i].charAt(j);
                }

            }
        }

        //convert char ke array
        char[] vokalArray = vokal.toCharArray();
        char[] konsonanArray = konsonan.toCharArray();

        //sorting
        Arrays.sort(vokalArray); //tipe data char //no out.print
        Arrays.sort(konsonanArray);

        //Print output
        System.out.print("Huruf vokal: ");
        System.out.println(vokalArray);
        System.out.print("Huruf konsonan: ");
        System.out.println(konsonanArray);

    }
}

