package Logic03;

import java.util.Scanner;

public class PlusMinus {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the number sequence: ");
        String numberString = input.nextLine();

        String[] numberStringArray = numberString.split(" ");
        int[] numberInt = new int[numberStringArray.length];
        int positive = 0;
        int negative = 0;
        int zero=0;

        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
        }

        //count the numbers = positif/negative/zero
        for (int i = 0; i < numberInt.length; i++) {
            if(numberInt[i]>0){
                positive=positive+1;
            }
            else if(numberInt[i]<0){
                negative=negative+1;
            }
            else {
                zero=zero+1;
            }
        }

        System.out.println("n positive: " + positive );
        System.out.println("n negative: " + negative);
        System.out.println("n zero: " + zero );

        float len = numberInt.length;
        System.out.print("The + proportion:");
        System.out.println(positive/len);
        System.out.print("The - proportion:");
        System.out.println(negative/len);
        System.out.print("The - proportion:");
        System.out.println(zero/len);
    }
}
