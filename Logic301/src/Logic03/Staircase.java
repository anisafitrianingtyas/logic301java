package Logic03;

import java.util.Scanner;

public class Staircase {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Height and width of n: ");
        int n = input.nextInt();

        //input *
        for (int i = 0; i < n; i++) {
            for(int j = i; j < n; j++){
                System.out.print(" ");
            }
            for(int j=0; j<=i; j++){
                System.out.print("#");
            }
            System.out.println();
        }

    }
}

