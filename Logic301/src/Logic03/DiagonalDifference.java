package Logic03;

import java.util.Scanner;

public class DiagonalDifference {
    public static void Resolve(){

        /*Scanner input = new Scanner(System.in);
        System.out.print("Masukan nilai = \n");
        int diagonal1 = 0;
        int diagonal2 =0;
        for (int i=0; i<3; i++){
            String matriks[] = input.nextLine().split(" ");
            diagonal1 = diagonal1 + Integer.parseInt(matriks[i]);
            diagonal2 = diagonal2 + Integer.parseInt(matriks[3-1-i]);
        }
        int hasil;
        hasil = Math.abs(diagonal1 - diagonal2);
        System.out.print(hasil);
        System.out.println();*/


        //Pre-r
        int i=0, j = 0, m, n;
        int matriks[][] = new int[10][10];

        //Input matriks dimension
        Scanner scan = new Scanner(System.in);
        System.out.print("Input n baris matriks: ");
        m = scan.nextInt();
        System.out.print("Input n kolom matriks: ");
        n = scan.nextInt();

        //Input nilai matriks
        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++) {
                matriks[i][j] = scan.nextInt();
            }
        }

        //Count diagonal sum
        int diagonal1=0;
        int diagonal2=0;
        for(i=0; i< m; i++){
            for(j=0; j< n; j++){
                // 1st diagonal
                if(i == j) {
                    diagonal1 += matriks[i][j];
                }
                // 2nd diagonal
                if(i + j == n - 1){
                    diagonal2 += matriks[i][j];
                }
            }
        }

        //print value
        int hasil;
        hasil=Math.abs(diagonal1-diagonal2);
        System.out.println(hasil);
    }
}
