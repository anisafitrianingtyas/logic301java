package Logic03;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while (answer.toUpperCase().equals("Y")) {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1:
                    SolveMeFirst.Resolve();
                    break;
                case 3:
                    SimpleArraySum.Resolve();
                    break;
                case 4:
                    DiagonalDifference.Resolve();
                    break;
                case 5:
                    PlusMinus.Resolve();
                    break;
                case 6:
                    Staircase.Resolve();
                    break;
                case 7:
                    MiniMaxSum.Resolve();
                    break;
                case 8:
                    BirthdayCakeCandles.Resolve();
                    break;
                case 9:
                    AVeryBigSum.Resolve();
                    break;
                case 10:
                    CompareTheTriplets.Resolve();
                    break;
                case 11:
                    Median.Resolve();
                    break;
                case 12:
                    Modus.Resolve();
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }
            System.out.println();
            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");
    }
}