package Logic03;

import javax.swing.*;
import java.util.Scanner;

public class SolveMeFirst {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the value of a: ");
        String numberStringA = input.nextLine();
        System.out.println("Enter the value of b: ");
        String numberStringB = input.nextLine();

        String[] numberStringArrayA = numberStringA.split(" ");
        String[] numberStringArrayB = numberStringB.split(" ");
        int[] numberIntA = new int[numberStringArrayA.length];
        int[] numberIntB = new int[numberStringArrayB.length];
        int answer = 0;

        for (int i = 0; i < numberIntA.length; i++) {
            numberIntA[i] = Integer.parseInt(numberStringArrayA[i]);
        }

        for (int i = 0; i < numberIntB.length; i++) {
            numberIntB[i] = Integer.parseInt(numberStringArrayB[i]);
        }


        for (int i = 0; i < numberIntA.length; i++) {
            answer = numberIntA[i]+numberIntB[i];
        }

        System.out.println("The answer is: " + answer);
    }
}
