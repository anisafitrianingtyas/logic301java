package Logic03;

import java.util.Scanner;

public class CompareTheTriplets {
    public static void Resolve() {
        Scanner in = new Scanner(System.in);
        //Input score Alice n Bob ke dalam array
        //a milik Alice n b milik Bob
        int alice0 = in.nextInt();
        int alice1 = in.nextInt();
        int alice2 = in.nextInt();
        int bob0 = in.nextInt();
        int bob1 = in.nextInt();
        int bob2 = in.nextInt();

        //Inisialisasi
        int aliceCount = 0;
        int bobCount = 0;

        //Perhitungan score
        //0
        if(alice0 > bob0){
            aliceCount++;
        }
        else if(alice0 < bob0){
            bobCount++;
        }
        //1
        if(alice1 > bob1){
            aliceCount++;
        }
        else if (alice1 < bob1){
            bobCount++;
        }
        //2
        if(alice2 > bob2){
            aliceCount++;
        }
        else if(alice2 < bob2) {
            bobCount++;
        }

        System.out.println(aliceCount + " " + bobCount);

        /*const aliceArray = a;
        const bobArray = b;
        let aliceTotal = 0;
        let bobTotal = 0;

        // both arrays have length of 3 so aliceArray.length
        // or bobArray.length will both work
        for (let i = 0; i < aliceArray.length; i++) {
            if (aliceArray[i] > bobArray[i]) {
                aliceTotal++; // alice gets a point
            } else if (aliceArray[i] < bobArray[i]) {
                bobTotal++; // bob gets a point
            } else {
                continue; // they both get zero points so skip
            }
        }
            return [aliceTotal, bobTotal];*/

        /*Scanner input = new Scanner(System.in);
        System.out.println("Please enter the value of the Triplets");
        int[][] array = new int[2][3];
        int a = 0;
        int b = 0;

        // enter value of array
        int i;
        int j;
        for(i = 0; i < array.length; ++i) {
            for(j = 0; j < array[0].length; ++j) {
                System.out.print("Enter values for row " + i + " and column " + j + ": ");
                int deret = input.nextInt();
                array[i][j] = deret;
            }
        }

        // prerequisite
        for(i = 0; i < array[0].length; ++i) {
            if (array[0][i] > array[1][i]) {
                a++;
            } else if (array[0][i] < array[1][i]) {
                b++;
            } else {
                //no additional point
                a = a;
                b = b;
            }
        }

        // print array
        for(i = 0; i < array.length; ++i) {
            for(j = 0; j < array[0].length; ++j) {
                System.out.print(array[i][j] + " ");
            }

            System.out.println();
        }

        System.out.println("Alice receives " + a + " points");
        System.out.println("Bob receives " + b + " points");*/

    }
}