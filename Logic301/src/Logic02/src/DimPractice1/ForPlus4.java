package DimPractice1;

import java.util.Scanner;

public class ForPlus4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		// Nomor 04
		System.out.println("Nomor 04");
		// Prompt input
		System.out.print("Input value of n:");
		int n = input.nextInt();

        // Prerequisite
        int[] arrayInteger = new int[n];
        int helper = 1;

        // Enter the value to array variable
        for (int i = 0; i < n; i++) {
            arrayInteger[i] = helper;
            helper += 4;
        }

        // Print array
        for (int i = 0; i < n; i++) {
            System.out.print(arrayInteger[i] + " ");
        }

        System.out.println();
        input.close();
	}

}
