package Logic02.src.DimPractice1;

import java.util.Scanner;
public class Prima {
    public static void main(String args[])
    {
        Scanner input = new Scanner(System.in);
        
        // Prompt input
     	System.out.print("Input value of n:");
     	int n = input.nextInt();
     	
        //Prerequisite
        int[] arrayInteger = new int[n];
        int helper;

        // Enter the value to array variable
        for (int i=0; i<=n; i++){
            helper=0;
            for (int j=1;j<=i;j++){
                if (i%j==0){
                	arrayInteger[helper] = i;
                    ++helper;
                }
            }
            if (helper==2){
                System.out.print(i + " ");
            }             
        }   
        
        // Print array
        for (int i = 0; i < n; i++) {
            System.out.print(arrayInteger[i] + " ");
        }

        System.out.println();
        input.close();
   }
}