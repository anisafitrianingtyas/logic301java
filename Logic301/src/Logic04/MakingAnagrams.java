package Logic04;

import java.util.Scanner;

public class MakingAnagrams {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input the text : ");
        String line1 = input.nextLine();
        String line2 = input.nextLine();

        int count1[] = new int[26];
        int count2[] = new int[26];

        for (int i=0;i<line1.length();i++){
            count1[line1.charAt(i)-'a']+=1;
        }

        for (int i=0;i<line2.length();i++){
            count2[line2.charAt(i)-'a']+=1;
        }

        int result = 0;
        for (int i = 0; i < 26; i++){
            result += Math.abs(count1[i] - count2[i]);
        }
        System.out.println();
        System.out.println("Output : ");
        System.out.println(result);
    }
}
