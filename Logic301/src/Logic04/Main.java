package Logic04;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while (answer.toUpperCase().equals("Y")) {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1:
                    CamelCase.Resolve();
                    //For example, s=oneTwoThree. There are  words in the string.
                    break;
                case 2:
                    DinamisStrongPassword.Resolve();
                    break;
                case 3:
                    CaesarChipher.Resolve();
                    //encrypting using a chiper.
                    break;
                case 4:
                    MarsExploration.Resolve();
                    //SOS
                    break;
                case 5:
                    HackerRank.Resolve();
                    //hackerrank word
                    break;
                case 6:
                    Pangrams.Resolve();
                    //it contains every letter of the alphabet
                    break;
                case 7:
                    SeparateTheNumbers.Resolve();
                    //A numeric string,
                    //is beautiful if it can be split into a sequence of two or more positive integers,
                    break;
                case 8:
                    Gemstones.Resolve();
                    //Given a list of minerals embedded in each of John's rocks,
                    // display the number of types of gemstones he has in his collection.
                    break;
                case 9:
                    MakingAnagrams.Resolve();
                    //For example,
                    // bacdc and dcbac are anagrams, but bacdc and dcbad are not.
                    break;
                case 10:
                    TwoStrings.Resolve();
                    //Given two strings, determine if they share a common substring.
                    // A substring may be as small as one character.
                    break;
                case 11:
                    Palindrome.Resolve();
                    //katak reversed katak
                    break;
                case 12:
                    Asteriks.Resolve();
                    //Saya -> S**a
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }
            System.out.println();
            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");
    }
}