package Logic04;

import java.util.Scanner;

public class HackerRank {
    public static void Resolve() {
        Scanner in = new Scanner(System.in);
        System.out.print("Cek Kata berapa kali?");
        int q = in.nextInt();
        for (int a0 = 0; a0 < q; a0++) {
            String s = in.next();

            int j = 0;    //keeps track of the letter index of the word "hackerrank"
            String hackerRank = "hackerrank";
            for (int i = 0; i < s.length(); i++) {
                if (j < hackerRank.length() && s.charAt(i) == hackerRank.charAt(j)) {
                    j++;
                }
            }

            //checks if all the letters of the word "hackerrank" have occured in the word given
            if (j == hackerRank.length())
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}
