package Logic06;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Soal05 {
    public static void Resolve() {
            Scanner input = new Scanner(System.in);
            System.out.print("Konversi Waktu");
            System.out.print("Masukan Jam (Ex: 13:30 ) : ");
            String StringJam = input.nextLine(); //Input waktu
            String[] ArrayStringJam = StringJam.split(" "); //Split stringjam antara AM/PM dan Jam

            if(ArrayStringJam.length==1){ //Kondisi ketika inputan panjangnya 1 maka untuk 12H ke 24H
                String StringJam24 = ArrayStringJam[0];
                String[] ArrayStringJam24 = StringJam24.split(":"); //Split Waktu berdasarkan ":"

                int jam24H = Integer.parseInt(ArrayStringJam24[0]); // Mengembalikan nilai jam string ke int
                int menit24H = Integer.parseInt(ArrayStringJam24[1]); // Mengembalikan nilai menit string ke int

                if (jam24H < 12 && menit24H <= 59) {
                    System.out.print("Output : " + jam24H + ":" + menit24H + " AM");
                } else if (jam24H > 12 && jam24H <= 23 && menit24H <= 59) {
                    System.out.print("Output : " + (jam24H - 12) + ":" + menit24H + " PM");
                } else if ((jam24H > 24) || (menit24H > 59)) {
                    System.out.print("Format tidak valid");
                }
            } else{ //Kondisi ketika inputan panjangnya 1 maka untuk 24H ke 12H
                String StringJam12 = ArrayStringJam[0];
                String[] ArrayStringJam12 = StringJam12.split(":"); //Split Waktu berdasarkan ":"

                int jam12H = Integer.parseInt(ArrayStringJam12[0]); // Mengembalikan nilai jam string ke int
                int menit12H = Integer.parseInt(ArrayStringJam12[1]); // Mengembalikan nilai menit string ke int

                String StringAM = "AM";
                String StringPM = "PM";

                String AMPM = ArrayStringJam[1]; // Menyimpan string AM/PM

                if (jam12H < 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                    System.out.print("Output : " + jam12H + ":" + menit12H);
                } else if (jam12H == 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                    System.out.print("Output : " + "00" + ":" + menit12H);
                } else if (jam12H < 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringPM.toLowerCase())) {
                    System.out.print("Output : " + (jam12H + 12) + ":" + menit12H);
                } else if ((jam12H > 12) || (menit12H > 59)) {
                    System.out.print("Format tidak valid");
                }
            }
        }

        /*Scanner input = new Scanner(System.in);

        char ulangKonversi;
        do {
            System.out.println("Ketik 1 jika ingin konversi 24H ke 12H");
            System.out.println("Ketik 2 jika ingin konversi 12H ke 24H");
            System.out.println("Konversi: ");
            int konversi = input.nextInt();

            //konversi 24H ke 12H
            if (konversi == 1) {
                System.out.println("Konversi 24H ke 12H");
                System.out.println("Format input jam, contoh: 03:04");
                System.out.println("Jam: 01");
                System.out.println("Menit: 04");
                System.out.print("Jam: ");
                int jam = input.nextInt();
                System.out.print("Menit: ");
                int menit = input.nextInt();
                System.out.print("AM/PM?: ");
                String waktu = input.next();
                int hasilJam = 0;

                //kondisi perhitungan
                if (jam > 12 && jam < 24) {
                    hasilJam = jam - 12;
                    if (menit >= 10)
                        System.out.println("Hasil Konversi jam: " +hasilJam+ ":" +menit+ " " +waktu+ "");
                    else if (menit < 10)
                        System.out.println("Hasil Konversi jam: " +hasilJam+ ":0" +menit+ " " +waktu+ "");

                } else if (jam <= 12) {
                    hasilJam = jam;
                    if (menit >= 10)
                        System.out.println("Hasil Konversi jam: " +hasilJam+ ":" +menit + " " +waktu+ "");
                    else if (menit < 10)
                        System.out.println("Hasil Konversi jam: " +hasilJam+ ":0" +menit+ " " +waktu+ "");

                } else if (jam == 24 && menit <=60) {
                    hasilJam = jam - 24;
                    System.out.println("Hasil Konversi jam: 0" +hasilJam+ ":" +menit+ " AM");
                } else {
                    System.out.println("Format salah.");
                }
            }

            //konversi 12H ke 24H
            else if (konversi == 2) {
                System.out.println("Konversi 12H ke 24H");
                System.out.println("Format penulisan jam, contoh: 12:05 PM");
                System.out.println("Dibaca jam 12 lebih 5 PM");
                System.out.print("Input jam=> jam: ");
                int jam = input.nextInt();
                System.out.print("lebih: ");
                int menit = input.nextInt();
                System.out.print("1. AM atau 2. PM?: ");
                int waktu = input.nextInt();

                //kondisi perhitungan
                int hasilJam = 0;
                if (jam > 0 && jam <= 12) {
                    if (waktu == 1) {
                        hasilJam = jam;
                        if (menit >= 10)
                            System.out.println("Hasil Konversi jam: " + hasilJam + ":" + menit + "");
                        else if (menit < 10)
                            System.out.println("Hasil Konversi jam: " + hasilJam + ":0" + menit + "");
                    } else if (waktu == 2) {
                        hasilJam = jam + 12;
                        if (menit >= 10)
                            System.out.println("Hasil Konversi jam: " + hasilJam + ":" + menit + "");
                        else if (menit < 10)
                            System.out.println("Hasil Konversi jam: " + hasilJam + ":0" + menit + "");
                    }
                } else if (jam == 0 && waktu == 1){
                    hasilJam = jam + 24;
                    System.out.println("Hasil Konversi jam: " +hasilJam+ ":00" +menit+ "");
                }
            }


            System.out.println("Konversi lagi?");
            ulangKonversi = input.next().charAt(0);
        }   while(ulangKonversi == 'y' && ulangKonversi != 'n');*/
    }

