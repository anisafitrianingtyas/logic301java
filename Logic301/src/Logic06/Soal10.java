package Logic06;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        //1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir.
        System.out.println("Sistem Konversi Volume");
        System.out.println("1. Botol");
        System.out.println("2. Gelas");
        System.out.println("3. Teko");
        System.out.println("4. Cangkir");
        System.out.print("Konversi : ");
        int konversi = input.nextInt();

        switch (konversi){
            case 1:
                //konversi 1 botol menjadi berapa
                System.out.print("Input jumlah botol: ");
                double jumlah = input.nextDouble();
                double totalGelas=jumlah*2;
                System.out.println("" +jumlah+ " botol = " +totalGelas+ " gelas");
                double totalCangkir=totalGelas*2.5;
                System.out.println("" +jumlah+ " botol = " +totalCangkir+ " cangkir");
                double totalTeko=totalCangkir/25;
                System.out.println("" +jumlah+ " botol = " +totalTeko+ " teko");
            case 2:
                //konversi 1 gelas menjadi berapa
                System.out.print("Input jumlah gelas: ");
                jumlah = input.nextDouble();
                double totalBotol=jumlah/2;
                System.out.println("" +jumlah+ " gelas = " +totalBotol+ " botol");
                totalCangkir=jumlah*2.5;
                System.out.println("" +jumlah+ " gelas = " +totalCangkir+ " cangkir");
                totalTeko=totalCangkir/25;
                System.out.println("" +jumlah+ " gelas = " +totalTeko+ " teko");
            case 3:
                //konversi 1 teko menjadi
                System.out.print("Input jumlah teko: ");
                jumlah = input.nextDouble();
                totalCangkir=jumlah*25;
                System.out.println("" +jumlah+ " teko = " +totalCangkir+ " cangkir");
                totalGelas=totalCangkir/2.5;
                System.out.println("" +jumlah+ " teko = " +totalGelas+ " gelas");
                totalBotol=totalGelas/2;
                System.out.println("" +jumlah+ " teko = " +totalBotol+ " botol");
            case 4:
                //konversi 1 cangkir menjadi
                System.out.print("Input jumlah cangkir: ");
                jumlah = input.nextDouble();
                totalTeko=jumlah/25;
                System.out.println("" +jumlah+ " cangkir = " +totalTeko+ " teko");
                totalGelas=jumlah/2.5;
                System.out.println("" +jumlah+ " cangkir = " +totalGelas+ " gelas");
                totalBotol=totalGelas/2;
                System.out.println("" +jumlah+ " cangkir = " +totalBotol+ " botol");
        }

    }
}
