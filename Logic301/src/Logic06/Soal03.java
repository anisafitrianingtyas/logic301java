package Logic06;

import java.util.Arrays;
import java.util.Scanner;

public class Soal03 {
    public static void Resolve() {
        //input banyak record
        Scanner input = new Scanner(System.in);
        System.out.print("Input banyak record penjualan : ");
        int banyakInput = input.nextInt();
        input.nextLine();
        //convert record penjualan ke dalam array
        String[] recordPenjualan = new String[banyakInput];
        System.out.println();
        //input record penjualan
        System.out.println("Penulisan record (ex. apel:3)");
        for (int i = 0; i < banyakInput; i++) {
            System.out.printf("Input record ke-" +i+ "= ");
            String inputPenjualanBuah = input.nextLine();
            if (inputPenjualanBuah.contains(":")) {
                recordPenjualan[i] = inputPenjualanBuah;
            } else {
                System.out.println("Salah format! Ulangi input dari awal.");
            }
        }
        System.out.println();

        //split ":" dalam inputan
        String[] buahList;
        String[][] recordPenjualanSplit = new String[banyakInput][2];
        for (int i = 0; i < banyakInput; i++) {
            buahList = recordPenjualan[i].split(":");
            for (int j = 0; j < buahList.length; j++) {
                recordPenjualanSplit[i][j] = buahList[j];
            }
        }

        //array yang berisi nama buahnya saja
        String[] buah = new String[banyakInput];

        //to lower case
        for (int i = 0; i < banyakInput; i++) {
            buah[i] = recordPenjualanSplit[i][0].toLowerCase();
        }

        //data yang sama dijadikan 1 atau remove duplicate
        //array yang berisikan nama buahnya saja tadi lalu di-remove duplicate
        //ex: jeruk, apel, apel -> jeruk, apel
        buahList = (String[]) Arrays.stream(buah).distinct().toArray((x$0) -> {
            return new String[x$0];
        });

        //array yang berisikan nama buah dan jumlah akhirnya
        String[][] summaryRecord = new String[buahList.length][2];

        Arrays.sort(buahList);
        for (int i = 0; i < buahList.length; i++) {
            int sumPenjualan = 0;
            for (int j = 0; j < recordPenjualanSplit.length; j++) {
                if (recordPenjualanSplit[j][0].toLowerCase().equals(buahList[i])) {
                    sumPenjualan += Integer.parseInt(recordPenjualanSplit[j][1]);
                    summaryRecord[i][0] = buahList[i];
                    summaryRecord[i][1] = Integer.toString(sumPenjualan);
                }
            }
        }

        //output summary record
        System.out.println("==Summary Record==");
        for (int i = 0; i < summaryRecord.length; i++) {
            for (int j = 0; j < summaryRecord[0].length; j++) {
                System.out.print(summaryRecord[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

    }
}

