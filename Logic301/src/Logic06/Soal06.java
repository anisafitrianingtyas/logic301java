package Logic06;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Soal06 {
    public static void Resolve(){
        //input tanggal dan hari pengiriman
        Scanner input = new Scanner(System.in);
        System.out.println("Ex. 20 november minggu");
        System.out.println("Input kapan pengiriman: ");
        String tanggalHariPengiriman = input.nextLine().toLowerCase();
        //di-split spasinya
        String[] tanggalHariPengirimanArray = tanggalHariPengiriman.split(" ");
        //lalu dimasukkan ke dalam array masing-masing tanggal dan harinya
        int tanggal = Integer.parseInt(tanggalHariPengirimanArray[0]);
        String hariPengiriman = tanggalHariPengirimanArray[1];

        //input tanggal hari libur
        System.out.println("Ex. 21 25");
        System.out.println("Input tanggal hari libur: ");
        String tanggalLiburString = input.nextLine();
        //di-split spasinya
        String[] tanggalLiburStringArray = tanggalLiburString.split(" ");
        //lalu dimasukkan ke dalam array tanggal liburnya
        int[] tanggalLibur = new int[tanggalLiburStringArray.length];
        for (int i = 0; i < tanggalLiburStringArray.length; i++) {
            tanggalLibur[i] = Integer.parseInt(tanggalLiburStringArray[i]);
        }

        //deklarasi untuk menentukan hari
        ArrayList hariList = new ArrayList();
        hariList.add("senin");
        hariList.add("selasa");
        hariList.add("rabu");
        hariList.add("kamis");
        hariList.add("jumat");
        hariList.add("sabtu");
        hariList.add("minggu");

        //deklarasi untuk menghitung hari pengiriman
        int holihari = 0;
        int hariKerja = 1;
        int hari = hariList.indexOf(hariPengiriman);
        String bulan = "";

        //perhitungan untuk menghitung tanggal pengiriman
        while (hariKerja <= 7) {
            if (tanggal > 31) {
                tanggal = Math.abs(31 - tanggal);
                bulan = "bulan berikutnya";
            }
            if (hari > 6) {
                hari = 0;
            }

            if (tanggal == tanggalLibur[holihari]) {
                tanggal++;
                hari++;
                if (tanggalLibur.length - 1 < holihari) {
                    holihari++;
                }
            } else if (hari == 5 || hari == 6) {
                hari++;
                tanggal++;
            } else {
                hari++;
                tanggal++;
                hariKerja++;
            }
        }
        System.out.println("Barang akan dikirimkan di tanggal " + tanggal + " " + hariList.get(hari) + " " + bulan);
    }
}
