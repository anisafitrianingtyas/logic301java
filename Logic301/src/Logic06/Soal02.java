package Logic06;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve(){
        //input jumlah rute
        Scanner input = new Scanner(System.in);
        System.out.println("1. Grosir X");
        System.out.println("2. Toko 1");
        System.out.println("3. Toko 2");
        System.out.println("4. Toko 3");
        System.out.println("5. Toko 4");
        System.out.println("Input jumlah rute: ");
        int rute = input.nextInt();

        //deklarasi
        int[] array = new int[rute];
        int jarak = 0;

        //input urutan rute
        int i;
        for(i = 0; i < array.length; i++) {
            System.out.print("Rute ke-" +i+ " adalah "); //i ini akan dihitung dari array ke [0]
            int pilihan = input.nextInt();
            array[i] = pilihan;
        }

        for(i = 0; i < array.length - 1; ++i) {
            if (array[i] == 1 && array[i + 1] == 2) {
                jarak += 500;
            } else if (array[i] == 1 && array[i + 1] == 3) {
                jarak += 2000;
            } else if (array[i] == 1 && array[i + 1] == 4) {
                jarak += 3500;
            } else if (array[i] == 1 && array[i + 1] == 5) {
                jarak += 5000;
            } else if (array[i] == 2 && array[i + 1] == 1) {
                jarak += 500;
            } else if (array[i] == 2 && array[i + 1] == 3) {
                jarak += 1500;
            } else if (array[i] == 2 && array[i + 1] == 4) {
                jarak += 3000;
            } else if (array[i] == 2 && array[i + 1] == 5) {
                jarak += 3500;
            } else if (array[i] == 3 && array[i + 1] == 1) {
                jarak += 2000;
            } else if (array[i] == 3 && array[i + 1] == 2) {
                jarak += 1500;
            } else if (array[i] == 3 && array[i + 1] == 4) {
                jarak += 1500;
            } else if (array[i] == 3 && array[i + 1] == 5) {
                jarak += 3000;
            } else if (array[i] == 4 && array[i + 1] == 1) {
                jarak += 3500;
            } else if (array[i] == 4 && array[i + 1] == 2) {
                jarak += 3000;
            } else if (array[i] == 4 && array[i + 1] == 3) {
                jarak += 1500;
            } else if (array[i] == 4 && array[i + 1] == 5) {
                jarak += 1500;
            } else if (array[i] == 5 && array[i + 1] == 1) {
                jarak += 5000;
            } else if (array[i] == 5 && array[i + 1] == 2) {
                jarak += 4500;
            } else if (array[i] == 5 && array[i + 1] == 3) {
                jarak += 3000;
            } else if (array[i] == 5 && array[i + 1] == 4) {
                jarak += 1500;
            }
        }

        //t=s/v laju rata-rata motor 30 km/jam = 30000/60 m/menit
        //waktu = jarak / kecepatan
        int waktuDijalan=jarak/(30000/60);
        System.out.println("Waktu perjalanan: " + waktuDijalan + " menit");
        int totalWaktu;
        //dikurang 2 karena rute yang diambil dari toko grosir x kembali ke x
        // dan dikali 10, karena waktu yang dibutuhkan disetiap toko adalah 10 menit
        totalWaktu=waktuDijalan+((rute-2)*10);
        System.out.println("Waktu yang dibutuhkan sampai kembali ke grosir " +totalWaktu+ " menit");
    }
}
