package Logic06;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve() {
        //input batas
        Scanner input = new Scanner(System.in);
        System.out.print("Input panjang deret/array: ");
        int panjang = input.nextInt();

        //deklarasi
        int kelipatanTiga = 3;
        int kelipatanDua = -2;

        //convert to array
        int[] arrayKelipatanTiga = new int[panjang];
        int[] arrayKelipatanTigaHasil = new int[panjang];
        int[] arrayKelipatanDua = new int[panjang];
        int[] arraySum = new int[panjang];

        int i;
        //kelipatan 3
        System.out.print("Deret kelipatan 3 - 1: ");
        for (i = 0; i < arrayKelipatanTiga.length; i++) {
            arrayKelipatanTiga[i] = kelipatanTiga;
            kelipatanTiga +=3;
        }

        //deret kelipatan 3-1
        for (i = 0; i < arrayKelipatanTigaHasil.length; i++) {
            arrayKelipatanTigaHasil[i] = arrayKelipatanTiga[i]-1;
            System.out.print(arrayKelipatanTigaHasil[i]+" ");
        }

        System.out.println();

        //kelipatan (-2)*1
        System.out.print("Deret kelipatan -2 x 1: ");
        for (i = 0; i < arrayKelipatanDua.length; i++) {
            System.out.print(kelipatanDua+" ");
            arrayKelipatanDua[i] = kelipatanDua;
            kelipatanDua+=(-2)*1;
        }

        System.out.println();

        System.out.print("Hasil penjumlahan: ");
        for (i = 0; i < arrayKelipatanDua.length; i++) {
            arraySum[i] = arrayKelipatanTigaHasil[i] + arrayKelipatanDua[i];
            System.out.print(arraySum[i] + " ");
        }

    }
}
