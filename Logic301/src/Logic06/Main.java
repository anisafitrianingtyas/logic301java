package Logic06;


import java.text.ParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while (answer.toUpperCase().equals("Y")) {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number) {
                case 1:
                    Soal01.Resolve(); //deret angka kelipatan 3 dikurang 1 dan kelipatan (-2) x 1
                    break;
                case 2:
                    Soal02.Resolve(); //waktu antar
                    break;
                case 3:
                    Soal03.Resolve(); //summary penjualan
                    break;
                case 4:
                    Soal04.Resolve(); //bisa beli barang apa saja
                    break;
                case 5:
                    Soal05.Resolve(); //ubah format jam dari 24H ke 12H dan sebaliknya
                    break;
                case 6:
                    Soal06.Resolve(); //barang sampai
                    break;
                case 7:
                    Soal07.Resolve(); //lompat
                    break;
                case 8:
                    Soal08.Resolve(); //beli barang dengan semaksimal uang
                    break;
                case 9:
                    Soal09.Resolve(); //tarif parkir
                    break;
                case 10:
                    Soal10.Resolve(); //konversi volume berdasarkan data
                    break;
                case 11:
                    Soal11.Resolve(); //point pembelian pulsa
                    break;
                case 12:
                    Soal12.Resolve(); //input string = bobot yang sesuai
                    break;
                case 13:
                    Soal13.Resolve(); //hitung panjang tali setelah dipotong
                    break;
                case 14:
                    Coba.Resolve();
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }
            System.out.println(" ");
            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Done");
    }
}
