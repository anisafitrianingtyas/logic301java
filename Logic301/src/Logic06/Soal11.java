package Logic06;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Keterangan point:");
        System.out.println("1. 0 - 10 ribu = 0 point");
        System.out.println("2. 10 - 30 ribu = 1 point, berlaku kelipatan 1 point setiap seribu");
        System.out.println("3. > 30 ribu = 1 point, berlalu kelipatan 2 point setiap kelipatan seribu");

        //input pulsa
        System.out.print("Beli pulsa Rp. ");
        int beliPulsa = input.nextInt();

        //hitung point
        int totalPulsaKelipatan1;
        int totalPulsaKelipatan2;
        int pointAkhir;
        int point=0;
        int pointKelipatan1;
        int pointKelipatan2;
        if(beliPulsa<=10000){ //jika pembelian kurang dari 10000
            pointAkhir=point;
            System.out.println("Anda mendapatkan " +pointAkhir+ " point");
        } else if (beliPulsa>10000 && beliPulsa <=30000) { //jika pembelian diantara 10000-30000
            totalPulsaKelipatan1=beliPulsa-10000; //dikurangi minimal beli untuk dapat 1 point
            pointKelipatan1=totalPulsaKelipatan1/1000; //kelipatan 1 point setiap seribu
            pointAkhir=point+pointKelipatan1;
            System.out.println("Anda mendapatkan " +point+ " + " +pointKelipatan1+ " = " +pointAkhir+ " point");
        } else if (beliPulsa>30000) { //jika pembelian lebih dari 30000
            totalPulsaKelipatan2=beliPulsa-30000; //min. beli untuk dapat 2 point
            pointKelipatan2=(totalPulsaKelipatan2/1000)*2; //kelipatan 2 point setiap seribu
            totalPulsaKelipatan1=(beliPulsa-totalPulsaKelipatan2) - 10000; //min. beli untuk dapat 1 point
            pointKelipatan1=totalPulsaKelipatan1/1000;
            pointAkhir=point+pointKelipatan1+pointKelipatan2;
            System.out.println("Anda mendapatkan " +point+ " + " +pointKelipatan1+ " + " +pointKelipatan2+ " = " +pointAkhir+ " point");
        }
    }
}
