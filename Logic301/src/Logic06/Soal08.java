package Logic06;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.print("Jumlah uang Andi yang dimiliki Rp. ");
        int uang = input.nextInt();
        System.out.print("Input banyak data harga: ");
        int banyakHarga = input.nextInt();
        input.nextLine();
        int[] array = new int[banyakHarga];
        int[] arrayHargaKacamata = new int[banyakHarga];
        int[] arrayHargaBaju = new int[banyakHarga];
        int[] arraySum = new int[banyakHarga];
        System.out.println();

        //input harga kacamata
        System.out.println("*Harga kacamata*");
        for (int i = 0; i < array.length; i++) {
            System.out.print("Input harga kacamata Rp. ");
            int hargaKacamata = input.nextInt();
            arrayHargaKacamata[i] = hargaKacamata;
        }
        //array harga kacamata
        for (int i = 0; i < array.length; ++i) {
            System.out.print(arrayHargaKacamata[i]+ ", ");
        }

        System.out.println();

        //input harga baju
        System.out.println("*Harga baju*");
        for (int i = 0; i < array.length; i++) {
            System.out.print("Input harga baju Rp. ");
            int hargaBaju = input.nextInt();
            arrayHargaBaju[i] = hargaBaju;
        }

        //array harga baju
        for (int i = 0; i < array.length; ++i) {
            System.out.print(arrayHargaBaju[i]+ ", ");
        }

        //perhitungan untuk mendapatkan harga pas untuk beli baju dan kacamata
        int helper=0;
        arraySum = new int[(arrayHargaKacamata.length+arrayHargaBaju.length)];
        for (int i = 0; i < arrayHargaKacamata.length; i++) {
            for (int j = 0; j < arrayHargaBaju.length; j++) {
                if (arrayHargaKacamata[i] + arrayHargaBaju[j] <= uang) {
                    arraySum[helper] = arrayHargaKacamata[i] + arrayHargaBaju[j];
                    helper++;
                }
            }
        }
        //Menacari nilai maksimum didalam array
        int max = arraySum[0];
        for(int i = 0; i < array.length; i++){
            if(arraySum[i] > max){
                max = arraySum[i];
            }
        }
        System.out.println("Output : ");
        System.out.println(max);

    }
}
