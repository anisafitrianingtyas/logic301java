package Logic06;
import java.util.Scanner;

public class Soal04 {
    public Soal04() {
    }

    public static void Resolve() {
        //input jumlah uang
        Scanner input = new Scanner(System.in);
        System.out.print("Input uang Andi: Rp.");
        int jumlahUang = input.nextInt();

        //input jumlah barang
        System.out.print("Input jumlah barang: ");
        int jumlahBarang = input.nextInt();

        //convert ke array
        int[] array = new int[jumlahBarang];
        int[] arrayHargaBarang = new int[jumlahBarang];
        String[] arrayNamaBarang = new String[jumlahBarang];
        System.out.println();

        //input nama barang dan harganya
        for(int i = 0; i < array.length; ++i) {
            System.out.print("Input Nama Barang = ");
            input.nextLine();
            String namaBarang = input.nextLine();
            arrayNamaBarang[i] = namaBarang;
            System.out.print("Input Harga Barang = Rp. ");
            int hargaBarang = input.nextInt();
            arrayHargaBarang[i] = hargaBarang;
            System.out.println();
        }

        String output = "";

        //cari barang yang bisa dibeli
        for(int i = 0; i < array.length; i++) {
            if (jumlahUang >= arrayHargaBarang[i]) {
                jumlahUang -= arrayHargaBarang[i];
                System.out.print("Barang-barang yang bisa dibeli Andi adalah ");
                output = output + arrayNamaBarang[i] + ", ";
            }
        }

        System.out.println(output);
        System.out.println("Sisa uang Andi sebanyak  " + jumlahUang + " Rupiah");
    }
}
